CC = gcc
CFLAGS = -ggdb -O0 -pedantic -Wall
LD = $(CC)
LDFLAGS = -ggdb

SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:.c=.o)

.PHONY:	all	clean

all:	sizes

sizes:	$(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f sizes $(OBJECTS)
