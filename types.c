#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdint.h>
#include <complex.h>

typedef struct {
    const char *name;
    size_t size;
} typename_t;

/*
 * each type and pointer-to-type
 */
#define ENTRY(Type)                             \
    { #Type, sizeof(Type) },                    \
    { #Type " *", sizeof(Type *) }

static
typename_t types[] =
    {
        ENTRY(char),
        ENTRY(unsigned char),
        ENTRY(short),
        ENTRY(short int),
        ENTRY(int),
        ENTRY(unsigned int),
        ENTRY(long),
        ENTRY(long int),
        ENTRY(float),
        ENTRY(double),
        ENTRY(long double),
        ENTRY(float complex),
        ENTRY(double complex),
    };
static const int n_types =
    sizeof(types) / sizeof(types[0]);

static
void
print_type_size(typename_t *t)
{
    printf("sizeof(%s) = %lu\n", t->name, (unsigned long) t->size);
}

void
find_type_size(const char *name)
{
    const size_t len = strlen(name);
    typename_t *walk = types;
    unsigned int i;

    for (i = 0; i < n_types; i += 1) {
        if (strncmp(walk->name, name, len) == 0) {
            print_type_size(walk);
            return;
            /* NOT REACHED */
        }
        walk += 1;
    }

    return;
}

void
print_type_sizes(void)
{
    typename_t *walk = types;
    unsigned int i;

    for (i = 0; i < n_types; i += 1) {
        print_type_size(walk);
        walk += 1;
    }
}
