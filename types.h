#ifndef _TYPES_H

#include <sys/types.h>

void find_type_size(const char *name);
void print_type_sizes(void);

#endif /* _TYPES_H */
