#include <stdio.h>

#include "types.h"

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        print_type_sizes();
    }
    else {
        int ai;

        for (ai = 1; ai < argc; ai += 1) {
            find_type_size(argv[ai]);
        }
    }

    return 0;
}
